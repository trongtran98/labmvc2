/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import da.ProductDataAccess;
import entity.Product;
import java.util.List;

/**
 *
 * @author Trong Tran
 */
public class ProductManagerBean {

    private Product product;
    ProductDataAccess productDataAccess = new ProductDataAccess();

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<Product> getListProduct() {
        return productDataAccess.getListProducts();
    }

    public Product getProductByID(){
        return productDataAccess.getProductByID(product.getId());
    }
    
    public boolean addProduct() {
        return productDataAccess.insertProduct(product);
    }

    public boolean delProduct() {
        return productDataAccess.delProduct(product.getId());
    }

    public boolean updateProduct() {
        return productDataAccess.updateProduct(product);
    }
}
