/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import da.UserDataAccess;
import entity.User;

/**
 *
 * @author Trong Tran
 */
public class LoginBean {

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    

    public boolean login() {
        return new UserDataAccess().login(user);
    }
}
