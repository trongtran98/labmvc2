/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import da.ProductDataAccess;
import entity.Product;
import java.util.List;

/**
 *
 * @author Trong Tran
 */
public class ProductFinderBean {

    private String keyword;

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public List<Product> getProducts() {
        return new ProductDataAccess().getProductsByName(keyword);
    }
}
