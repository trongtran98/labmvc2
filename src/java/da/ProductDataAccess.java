/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package da;

import entity.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Trong Tran
 */
public class ProductDataAccess {

    private PreparedStatement searchStatement, addStatement, getListProductStatement, getProductStatement, delStatement, updateStatement;

    private PreparedStatement getSearchStatement() throws ClassNotFoundException, SQLException {
        if (searchStatement == null) {
            Connection connection = new DBConnection().getConnection();
            searchStatement = connection.prepareStatement("Select pro_id,pro_name,pro_desc From ProductStore Where pro_name Like ?");
        }
        return searchStatement;
    }

    public List<Product> getProductsByName(String name) {
        try {
            PreparedStatement statement = getSearchStatement();
            statement.setString(1, "%" + name + "%");
            ResultSet rs = statement.executeQuery();
            List<Product> products = new LinkedList<Product>();
            while (rs.next()) {
                products.add(new Product(rs.getString("pro_id"), rs.getString("pro_name"), rs.getString("pro_desc")));
            }
            return products;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private PreparedStatement getAddStatement() throws ClassNotFoundException, SQLException {
        if (addStatement == null) {
            Connection connection = new DBConnection().getConnection();
            addStatement = connection.prepareStatement("Insert into ProductStore Values(?,?,?)");
        }
        return addStatement;
    }

    public boolean insertProduct(Product product) {
        try {
            PreparedStatement statement = getAddStatement();
            statement.setString(1, product.getId());
            statement.setString(2, product.getName());
            statement.setString(3, product.getDesc());
            int result = statement.executeUpdate();
            return result > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    private PreparedStatement getListProductsStatement() throws ClassNotFoundException, SQLException {
        if (getListProductStatement == null) {
            Connection connection = new DBConnection().getConnection();
            getListProductStatement = connection.prepareStatement("Select pro_id,pro_name,pro_desc From ProductStore");
        }
        return getListProductStatement;
    }

    public List<Product> getListProducts() {
        try {
            PreparedStatement statement = getListProductsStatement();
            ResultSet rs = statement.executeQuery();
            List<Product> products = new LinkedList<Product>();
            while (rs.next()) {
                products.add(new Product(rs.getString("pro_id"), rs.getString("pro_name"), rs.getString("pro_desc")));
            }
            return products;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private PreparedStatement getProductStatement() throws ClassNotFoundException, SQLException {
        if (getProductStatement == null) {
            Connection connection = new DBConnection().getConnection();
            getProductStatement = connection.prepareStatement("Select pro_id,pro_name,pro_desc From ProductStore where pro_id = ?");
        }
        return getProductStatement;
    }

    public Product getProductByID(String id) {
        try {
            PreparedStatement statement = getProductStatement();
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();
            Product product = null;
            while (rs.next()) {
                product = new Product(rs.getString("pro_id"), rs.getString("pro_name"), rs.getString("pro_desc"));
            }
            return product;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private PreparedStatement delStatement() throws ClassNotFoundException, SQLException {
        if (delStatement == null) {
            Connection connection = new DBConnection().getConnection();
            delStatement = connection.prepareStatement("DELETE FROM ProductStore where pro_id = ?");
        }
        return delStatement;
    }

    public boolean delProduct(String id) {
        try {
            PreparedStatement statement = delStatement();
            statement.setString(1, id);
            int result = statement.executeUpdate();
            return result > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    private PreparedStatement updateStatement() throws ClassNotFoundException, SQLException {
        if (updateStatement == null) {
            Connection connection = new DBConnection().getConnection();
            updateStatement = connection.prepareStatement("update ProductStore set pro_name = ?,pro_desc = ? where pro_id = ?");
        }
        return updateStatement;
    }

    public boolean updateProduct(Product product) {
        try {
            PreparedStatement statement = updateStatement();
            statement.setString(1, product.getName());
            statement.setString(2, product.getDesc());
            statement.setString(3, product.getId());
            int result = statement.executeUpdate();
            return result > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

 
}
