/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package da;

import entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Trong Tran
 */
public class UserDataAccess {

    private PreparedStatement loginStatement;

    private PreparedStatement loginStatement() throws ClassNotFoundException, SQLException {
        if (loginStatement == null) {
            Connection connection = new DBConnection().getConnection();
            loginStatement = connection.prepareStatement("select * from users where username=? and password =?");
        }
        return loginStatement;
    }

    public boolean login(User u) {
        try {
            PreparedStatement statement = loginStatement();
            statement.setString(1, u.getUsername());
            statement.setString(2, u.getPassword());
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return true;
            }
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
