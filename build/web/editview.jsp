<%-- 
    Document   : editview
    Created on : Feb 28, 2018, 7:41:19 PM
    Author     : Trong Tran
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${sessionScope.admin == null}">
    <c:redirect url="loginpage.jsp"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            span{display: inline-block; width: 40px; height: 25px;}
            input{width: 150px;}
        </style>
    </head>

    <body>
        <h1>Edit Page!</h1>
        <form method="post" action="ProductUpdater">
            <span>Id</span> <input name="id" readonly value="${requestScope.product.id}"/><br/>
            <span>Name</span> <input name="name" value="${requestScope.product.name}"/><br/>
            <span>Desc</span> <input name="desc" value="${requestScope.product.desc}"/><br/>
            <span>.</span><input style="width: 60px;" type="submit" value="update"/>
            <p style="color: red;">${requestScope.msg}</p>
        </form>
    </body>
</html>
