<%-- 
    Document   : add
    Created on : Feb 27, 2018, 5:53:49 PM
    Author     : Trong Tran
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${sessionScope.admin == null}">
    <c:redirect url="loginpage.jsp"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Page</title>
        <style>
            span{display: inline-block; width: 40px; height: 25px;}
            input{width: 150px;}
        </style>
    </head>
    <body>
        
        <h1>New Product!</h1>
        <form action="ProductAdder" method="post">
            <span>Id</span> <input name="id"/><br/>
            <span>Name</span> <input name="name"/><br/>
            <span>Desc</span> <input name="desc"/><br/>
            <span>.</span><input style="width: 50px;" type="submit" value="add"/>
        </form>
    </body>
</html>
