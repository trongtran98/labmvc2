<%-- 
    Document   : getlistproduct
    Created on : Feb 28, 2018, 12:52:34 AM
    Author     : Trong Tran
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${sessionScope.admin == null}">
    <c:redirect url="loginpage.jsp"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>List Products!</h1>
        <jsp:useBean class="model.ProductManagerBean" id="getter" scope="request"/>
        <span style="color: red;">${requestScope.msg}</span>
        <table>
            <thead>
            <th>Id</th>
            <th>Name</th>
            <th>Descriptor</th>
        </thead>
        <tbody>
            <c:forEach items="${getter.listProduct}" var="product">
                <tr>
                    <td><c:out value="${product.id}"/></td>
                    <td><c:out value="${product.name}"/></td>
                    <td><c:out value="${product.desc}"/></td>
                    <td><a href="ProductEditer?id=${product.id}">edit</a></td>
                    <td><a href="ProductRemover?id=${product.id}">remover</a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</body>
</html>
