<%-- 
    Document   : result
    Created on : Feb 27, 2018, 3:38:57 PM
    Author     : Trong Tran
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${sessionScope.admin == null}">
    <c:redirect url="loginpage.jsp"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List Product</title>
    </head>
    <body>
        <h1>List Product!</h1>
        <a href="search.jsp">Search</a>
        <jsp:useBean class="model.ProductFinderBean" scope="request" id="finder"/>
        
        <table border="1">
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Descriptor</td>
            </tr>
            <c:forEach items="${finder.products}" var="product">
                <tr>
                    <td><c:out value="${product.id}"/></td>
                    <td><c:out value="${product.name}"/></td>
                    <td><c:out value="${product.desc}"/></td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
