<%-- 
    Document   : search
    Created on : Feb 27, 2018, 3:36:35 PM
    Author     : Trong Tran
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:if test="${sessionScope.admin == null}">
    <c:redirect url="loginpage.jsp"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Shopping!</title>
    </head>
    <body>
        <h2>Welcome ${sessionScope.admin}!</h2>
        <a href="add.jsp">New product</a> | <a href="getlistproduct.jsp">List products</a> | <a href="/LabMVC2/Logout">Logout</a>
        <form action="ProductFinder">
            <span style="color: red;">
                <c:out value="${param.msg}"/>
            </span>
            <input name="name"/>&nbsp;<input type="submit" value="Search"/>
        </form>
    </body>
</html>
