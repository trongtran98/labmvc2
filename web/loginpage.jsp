<%-- 
    Document   : loginpage
    Created on : Mar 29, 2018, 3:20:06 PM
    Author     : Trong Tran
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <style>
            span{display: inline-block; width: 70px; height: 25px;}
            input{width: 150px;}
        </style>
        <form action="Login" method="post">
            <span> Username: </span><input name="username"/><br/>
            <span> Password: </span><input type="password" name="password"/><br/>
            <span>.</span><button>Login</button>
        </form>
        <p style="color: red;">${msg}</p>
    </body>
</html>
